# John Williams' Adventure

John Williams' Adventure (JWLA for short) is a libre FPS game based on the DOOM engine. All this does is provide assets, you need to use a source port to play JWLA.

Freedoom is to Doom as JWLA is to Chex Quest. The game mainly provides IWADs for The Ultimate Doom, and a bring-your-own-levels Doom 2 IWAD.

## How to Play

First, find a DOOM source port. Some good suggestions are:

- Chocolate Doom: purist source port that is as close to DOS Doom as can be.
- Crispy Doom: like Chocolate Doom but with several enhancements for a more consoomerist experience.
- DSDA Doom: A more advanced source port, extending level capabilities and the likes.

As JWLA is not recognized by any of these, you'll need to either:
- Use the command line to force-load JWLA as an IWAD (RECOMMENDED): Use the -iwad parameter on your source port of choice to force-load JWLA's WAD files as an IWAD. This means that, instead of loading JWLA on top of, say, Freedoom, it only loads JWLA. For example, ```chocolate-doom -iwad JWLA.wad```
- Rename JWLA.wad to doom.wad: A crude way of doing it, but it works. As JWLA is made primarily for *Freedoom: Phase 1*/*The Ultimate Doom*, you can safely rename JWLA.wad to doom.wad to trick your source port into loading it without a fuss. Depending on the source port, text strings may not match JWLA's intended replacements.

## How to Build

JWLA uses Deutex to automatically "compile" its WAD files. **You MUST bring your own version of Deutex!** To keep the reposity CC0, Deutex and miniwad (the blank IWAD JWLA uses for a base) are not distributed with the repository. Just add a ``doom2.wad`` to the root directory (preferably miniwad or Freedoom: Phase 1) and execute MAKE.sh to build.

Make sure you have deutex.exe (or just "deutex" for Unix/Linux) in the DEUTEX/ folder and use the build.bat/build.sh scripts.

## FAQ

### Isn't this derived off of some shitty game made by this kid?

Yes. **I was that kid.** I am the one who created the idea, the sprites, and the modification in 2018 and posted it on a site, hoping to work on it more and make a whole game with the concept. For proof, listen to the voice of the kid who made the game and compare it to mine. You can easily tell that I am him, just older and not idiotic.

As I have incidentally created it, I should have the full power to waive my remaining rights to the game's original contents. However, since it was both based on Chex Quest 3, a proprietary IWAD, and it was dogshit, I decided to remake it into a fully libre, not-as-awful version.
